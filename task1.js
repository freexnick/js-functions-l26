const curlingResults = [
  {
    team1: { name: "powerRangers", score: [44, 23, 71] },
    team2: { name: "fairyTails", score: [85, 54, 41] },
  },
  {
    team1: { name: "powerRangers", score: [65, 54, 49] },
    team2: { name: "fairyTails", score: [23, 34, 47] },
  },
];

//1.1. შექმენათ ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
const calcAverage = (arr) => {
  return arr.reduce((acc, val) => acc + val) / arr.length;
};

//1.2. გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
for (result of curlingResults) {
  result.team1.avg = calcAverage(result.team1.score);
  result.team2.avg = calcAverage(result.team2.score);
}

/*1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner), 
მიიღებს ორ პარამეტრს გუნდების საშუალოს სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით (მაგ. პირველმა გუნდმა გაიმარჯვა 31 vs 15 ქულა). */
const checkWinner = (obj) => {
  if (obj.team1.avg > obj.team2.avg) {
    obj.winner = obj.team1.name;
    console.log(
      `${obj.team1.name} won the game with the score of ${obj.team1.avg} to ${obj.team2.avg}`
    );
  } else if (obj.team2.avg > obj.team1.avg) {
    obj.winner = obj.team2.name;
    console.log(
      `${obj.team2.name} won the game with the score of ${obj.team2.avg} to ${obj.team1.avg}`
    );
  } else {
    obj.result = "Tie";
    console.log(
      `it's a tie with the score of ${obj.team1.avg} to ${obj.team2.avg}`
    );
  }
};
/* 1.4 გამოყენეთ ეს ფუნქცია რომ გამოავლინოთ გამარჯვებული გუნდი 1. PowerRangers (44, 23, 71) , FairyTails (65, 54, 49) 2. PowerRangers (85, 54, 41) , FairyTails (23, 34, 47)
 1.5 optional : ფუნქციაში შეგიძლიათ გაითვალისწინოთ ყაიმის შემთხვევაც. */
for (result of curlingResults) {
  checkWinner(result);
}
