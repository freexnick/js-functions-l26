/*2.2. შექმეათ მასივი სადაც შეინახავთ ანგარიშებს : 22, 295, 176, 440, 37, 105, 10, 1100, 96, 52 
(აქ ლუფით შენახვა არაა აუცილებელი, ეს მოცემული დატაა და პირდაპირ შეგიძლიათ ჩაწეროთ ერეიში) */
const Ludovico = {
  spent: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
};

// 2.1. შექმენათ ფუქნცია (calcTip) რომელიც მიიღებს ანგარიშის მნიშნელობას პარამეტრად და დააბრუნებს თიფს ზემოთხსენებული წესების მიხედვით
const calcTip = (arr) => {
  const result = arr.map((num) => {
    if (num >= 50 && num <= 300) {
      return num * 0.15;
    } else {
      return num * 0.2;
    }
  });
  return result;
};

//2.3. შექმენათ თიფების მასივი და რომელშიც შეინახავთ ფუნქციის მეშვეობით დათვლის თიფებს.
Ludovico.tip = calcTip(Ludovico.spent);

/*2.4. შექმენათ მასივი სადაც შეინახავთ ჯამურ თანხას თითოეული ანგარიში + ანგარიშის შესაბამისი თიფი.
2.5. გამოიყენოთ ლუფი კალკულაციების ჩასატარებლად.*/
Ludovico.total = Ludovico.spent.map((num1, num2) => {
  return num1 + Ludovico.tip[num2];
});

/* 2.6. შექმენათ ფუნქცია რომელიც მიიღებს მასივს პარამეტრად და დააბრუნებს საშუალოს, 
ამ ფუნქციით უნდა დავითვალოთ საშუალოდ რამდენ ჩაის ტოვებს ჩვენი ლუდოვიკო (2.3. პუნტში მიღებული მასივი) 
და ასევე უნდა დავითვალოთ საშუალოდ რა თანხა დახარჯა მანდ ჭამა-სმაში (2.4. პუნქტის მასივის საშუალო არითმეტიკული) */

const calcAverage = (arr) => {
  let sum = arr.reduce((acc, val) => acc + val);
  let result = sum / arr.length;
  return result;
};

Ludovico.totalAvg = calcAverage(Ludovico.total);
Ludovico.tipAvg = calcAverage(Ludovico.tip);

console.log(Ludovico);
